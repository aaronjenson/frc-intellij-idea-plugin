/*
 * Copyright 2015-2016 Mark Vedder
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.javaru.iip.frc.actions.create.ui.forms;

import java.awt.*;
import javax.swing.*;

import org.jetbrains.annotations.NotNull;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.ui.components.JBLabel;
import com.intellij.uiDesigner.core.Spacer;



public class DescriptionPanelBean implements PanelBean
{
    private static final Logger LOG = Logger.getInstance(DescriptionPanelBean.class);


    public DescriptionPanelBean(@NotNull JComponent content, String description)
    {
        myContentPanel.add(content);
        myDescriptionLabel.setText(description);
    }


    @NotNull
    @Override
    public JPanel getRootPanel() { return myRootPanel; }


    private JPanel myRootPanel;
    private JPanel myDescriptionPanel;
    private JBLabel myDescriptionLabel;
    private JPanel myContentPanel;


    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$()
    {
        myRootPanel = new JPanel();
        myRootPanel.setLayout(new BorderLayout(0, 0));
        myDescriptionPanel = new JPanel();
        myDescriptionPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
        myRootPanel.add(myDescriptionPanel, BorderLayout.NORTH);
        myDescriptionPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0), null));
        myDescriptionLabel = new JBLabel();
        myDescriptionLabel.setFont(new Font(myDescriptionLabel.getFont().getName(), Font.ITALIC, myDescriptionLabel.getFont().getSize()));
        myDescriptionPanel.add(myDescriptionLabel);
        final Spacer spacer1 = new Spacer();
        myDescriptionPanel.add(spacer1);
        myContentPanel = new JPanel();
        myContentPanel.setLayout(new BorderLayout(0, 0));
        myRootPanel.add(myContentPanel, BorderLayout.CENTER);
        myContentPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), null));
    }


    /** @noinspection ALL */
    public JComponent $$$getRootComponent$$$() { return myRootPanel; }
}
