/*
 * Copyright 2015-2016 Mark Vedder
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.javaru.iip.frc.actions.create.ui;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.Border;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;

import net.javaru.iip.frc.actions.create.ui.forms.FrcWizardMasterPanel;
import net.javaru.iip.frc.actions.create.ui.forms.ModelPanelBean;



public class FrcWizardDialog extends DialogWrapper
{

    private static final Logger LOG = Logger.getInstance(FrcWizardDialog.class);

    @Nullable
    private final Project project;
    private final JPanel myPanel;


    //TODO convert to Builder
    public FrcWizardDialog(@Nullable Project project,
                           @NotNull ModelPanelBean modelPanelBean,
                           @NotNull String taskTitle,
                           @Nullable String taskDescription)
    {
        super(project);
        this.project = project;
        
        final Border border = BorderFactory.createEmptyBorder(20, 0, 20, 0);
        final JPanel contentPanel = modelPanelBean.getRootPanel();
        contentPanel.setBorder(border);
        contentPanel.setPreferredSize(new Dimension(400, 80));
        contentPanel.revalidate();
        contentPanel.repaint();

        final FrcWizardMasterPanel masterPanel = new FrcWizardMasterPanel(project, modelPanelBean, taskTitle, taskDescription);
        this.myPanel = masterPanel.getRootPanel();
        init();
    }
    

    @Nullable
    @Override
    protected JComponent createCenterPanel()
    {
        return myPanel;
    }


    
}
